import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  title = "video-List";
  // videoList = ["itemm 1", "itemm 2", "itemm 3", "itemm 4"]
  videoList = [
    {
    name: "item 1",
    slug: "item-1"
    },
    {
      name: "item 2",
      slug: "item-2"
    },
    {
      name: "item 3",
      slug: "item-3"
    },
];

  constructor() { }

  ngOnInit() {
  }

}
